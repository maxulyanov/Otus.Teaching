﻿namespace SolidPrincipals.Games
{
    /// <summary>
    /// Интерфейс методов вывода данных
    /// </summary>
    public interface IGameWriter
    {
        /// <summary>
        /// Вывод заголовка
        /// </summary>
        /// <param name="text">строка заголовка</param>
        void PrintHeadline(string text);

        /// <summary>
        /// Вывод текста успеха
        /// </summary>
        /// <param name="text">строка для вывода</param>
        void PrintSuccess(string text);

        /// <summary>
        /// Вывод текста ошибки
        /// </summary>
        /// <param name="text">строка для вывода</param>
        void PrintError(string text);

        /// <summary>
        /// Вывод текста ошибки
        /// </summary>
        /// <param name="text">строка для вывода</param>
        void PrintStandart(string text);

        /// <summary>
        /// Вывод текста вопроса
        /// </summary>
        /// <param name="text">строка для вывода</param>
        void PrintQuestion(string text);

        /// <summary>
        /// Метод очистки всех данных выведенных ранее
        /// </summary>
        void EraseAllData();
    }
}
