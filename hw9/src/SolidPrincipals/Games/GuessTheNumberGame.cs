﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SolidPrincipals.Models;

namespace SolidPrincipals.Games
{
    internal class GuessTheNumberGame : IGame
    {
        readonly IGameReader _gameReader;
        readonly IGameWriter _gameWriter;
        readonly ILogger<GuessTheNumberGame> _logger;
        readonly GuessTheNumber _options;
        private bool _isStoped;
        private int _tryCount;

        public string Name => "Угадай число";

        public GuessTheNumberGame(IGameReader gameReader, IGameWriter gameWriter, IConfiguration options, ILogger<GuessTheNumberGame> logger)
        {
            _gameReader = gameReader;
            _gameWriter = gameWriter;
            _logger = logger;
            _options = options.GetSection(nameof(GuessTheNumber)).Get<GuessTheNumber>();

            _logger.LogDebug($"GuessTheNumberGame options: {_options}");
        }

        public void Epilogue()
        {
            _logger.LogDebug("Epilogue run");

            if (_isStoped)
            {
                _gameWriter.PrintError("Игра была остановлена\r\n");
                return;
            }

            if (_tryCount < 0)
            {
                _gameWriter.PrintError("Вы проиграли\r\n");
            }
            else
            {
                _gameWriter.PrintSuccess("Вы угадали! Поздравляем!\r\n");
                _gameWriter.PrintStandart($"У вас оставалось {_tryCount} попыток\r\n");
            }
        }

        public void Intro()
        {
            _logger.LogDebug("Intro run");

            _gameWriter.EraseAllData();
            _gameWriter.PrintHeadline("Игра 'Угадай число'");
            _gameWriter.PrintStandart($"Необходимо угадать число из диапазона от '{_options.MinNumber}' до '{_options.MaxNumber}'\r\n");
            _gameWriter.PrintStandart($"Всего попыток '{_options.TryCount}'\r\n");
        }

        public void RunGame()
        {
            _logger.LogDebug("RunGame");

            _isStoped = false;
            int guessedValue = GetRandomInt();
            _tryCount = _options.TryCount;

            _logger.LogDebug($"Guessed value is {guessedValue}");

            while (!_isStoped && _tryCount-- > 0)
            {
                _gameWriter.PrintQuestion("Ваш вариант числа: ");

                if (ReadGueussInt(guessedValue))
                {
                    return;
                }
            }
        }

        public void StopGame()
        {
            _logger.LogDebug("StopGame");
            _isStoped = true;
        }

        private bool ReadGueussInt(int guessedValue)
        {            
            try
            {
                int guess = _gameReader.ReadInt();
                if (guess < guessedValue)
                    _gameWriter.PrintError($"Ваше число МЕНЬШЕ чем загаданное\r\n");
                if (guess > guessedValue)
                    _gameWriter.PrintError($"Ваше число БОЛЬШЕ чем загаданное\r\n");

                return guess == guessedValue;
            }
            catch (Exception ex)
            {
                _logger.LogDebug(ex, "ReadGueussInt error");
                _gameWriter.PrintError($"Введено некорректное число, допустим ввод числа в диапазоне от {int.MinValue} до {int.MaxValue}\r\n");
            }

            return false;
        }

        private int GetRandomInt()
        {
            var random = new Random();
            return random.Next(_options.MinNumber, _options.MaxNumber);
        }
    }
}
