﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SolidPrincipals.Games;
using System.Reflection;

namespace SolidPrincipals.Services
{
    internal class GameService : BackgroundService, IDisposable
    {
        readonly ILogger<GameService> _logger;
        readonly IGameReader _gameReader;
        readonly IGameWriter _gameWriter;
        readonly IServiceProvider _serviceProvider;
        private List<IGame>? _games;

        public GameService(IGameReader gameReader, IGameWriter gameWriter, IServiceProvider serviceProvider, ILogger<GameService> logger)
        {
            _logger = logger;
            _gameReader = gameReader;
            _gameWriter = gameWriter;
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Метод получения всех определёных игр в приложении
        /// </summary>
        private void ListAllGames()
        {
            if (_serviceProvider != null)
            {
                _games ??= new List<IGame>();
                Assembly assembly = Assembly.GetExecutingAssembly();
                foreach (Type type in assembly.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(IGame))))
                {
                    if (ActivatorUtilities.CreateInstance(_serviceProvider, type) is IGame game)
                        _games.Add(game);
                }
            }
        }

        /// <summary>
        /// Метод вывода списка игр
        /// </summary>
        private bool GameMenu()
        {
            _gameWriter.EraseAllData();
            _gameWriter.PrintHeadline("Доступный список игр");
            if (_games?.Count > 0)
            {
                for (int i = 0; i < _games.Count; i++)
                {
                    _gameWriter.PrintSuccess($"{i + 1}. '{_games[i].Name}'\r\n");
                }
                _gameWriter.PrintQuestion("\r\nВыберите игру: ");

                return true;
            }
            else
            {
                _gameWriter.PrintError("Игр не найдено\r\n");
            }

            return false;
        }

        /// <summary>
        /// Метод выбора игры и её запуск
        /// </summary>
        /// <returns></returns>
        private async Task<bool> SelectGame()
        {
            try
            {
                int gameNum = _gameReader.ReadInt() - 1; // индекс игр у нас начинается с 1 в интерфейсе

                if (gameNum >= 0 && gameNum < _games?.Count)
                {
                    await Task.Run(() =>
                    {
                        _games[gameNum].Intro();
                        _games[gameNum].RunGame();
                        _games[gameNum].Epilogue();
                    });

                    return true;
                }
            }
            catch (Exception)
            {
                _gameWriter.PrintError("Некорректно введён номер игры");
            }

            return false;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            ListAllGames();

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    if (GameMenu())
                    {
                        await SelectGame();
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "GameService.ExecuteAsync: main error");
                }

                _gameWriter.PrintQuestion("Вы хотите выйти (y/n)? ");
                if (_gameReader.ConfirmationDialog())
                {
                    break;
                }
            }

            Environment.Exit(0);
        }

        #region IDispose

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                if (_games?.Count > 0)
                {
                    foreach (var game in _games)
                        game.StopGame();

                    _games = null;
                }

                disposed = true;
            }
        }

        public override void Dispose()
        {
            Dispose(true);
            base.Dispose();

            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
