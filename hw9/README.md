# SOLID principals, homework 9

1. **S** Классы отвечающие данному принципу: GameService (общее управление и выбор игры), GuessTheNumberGame (логика игры), ConsoleWriter (вывод информации), ConsoleReader (ввод информации), Program (настройки и запуск приложения)
2. **O** Применение данного принципа в классах: GameService (переопределение Dispose базового класса BackgroundService, поддержка дополнительных игр методом расширения по общему интерфейсу в приватном методе ListAllGames), GuessTheNumber (переопределение ToString). Принцип закрытости используется во всех классах
3. **L** Философия поддерживается при реализации всех интерфейсов программы.
4. **I** Например IGameReader и IGameWriter вместо объеденённого IGameInputOutput
5. **D** Ни в одном классе нет жёсткой зависимости (пример паттерна инициализации класса GuessTheNumberGame(IGameReader gameReader, IGameWriter gameWriter, IConfiguration options, ILogger<GuessTheNumberGame> logger))
