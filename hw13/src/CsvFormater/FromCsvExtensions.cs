﻿using System.Collections;
using System.Reflection;
using System.Text;

namespace CsvFormater
{
    public static class FromCsvExtensions
    {
        /// <summary>
        /// Вспомогательный метод разбивки текста на строки
        /// </summary>
        /// <param name="str"></param>
        /// <param name="removeEmptyLines">удалять или нет пустые строки из результата</param>
        /// <returns></returns>
        private static IEnumerable<string> GetLines(string str, bool removeEmptyLines = false)
        {
            if (string.IsNullOrEmpty(str))
                return Enumerable.Empty<string>();

            return str.Split(new[] { "\r\n", "\r", "\n" }, removeEmptyLines ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None);
        }

        /// <summary>
        /// Вспомогательный метод получения значений из строки CSV с экранированными символами (запятая в текстовом поле)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static IEnumerable<string> GetValuesFromCsvLine(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return Enumerable.Empty<string>();

            List<string> result = new();
            bool isString = false;
            StringBuilder sb = new();

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == ',' && !isString)
                {
                    result.Add(sb.ToString());
                    sb.Clear();
                }
                else
                {
                    // обрабатываем экранирование строки
                    if (!isString && str[i] == '"' && (i == 0 || str[i - 1] == ','))
                    {
                        isString = true;
                    }
                    else if (isString && str[i] == '"' && (i + 1 == str.Length || str[i + 1] == ','))
                    {
                        isString = false;
                    }
                    else
                    {
                        sb.Append(str[i]);
                    }
                }
            }

            if ((!isString && sb.Length > 0) || str[^1] == ',')
            {
                result.Add(sb.ToString());
            }

            return result;
        }

        /// <summary>
        /// Вспомогательный метод десериализации строки CSV в объект указанного типа
        /// </summary>
        /// <param name="type">ожидаемый тип</typeparam>
        /// <param name="str">строка значений</param>
        /// <param name="csvProps">упорядоченный список обозначенных свойств в CSV файле</param>
        /// <param name="str">ожидаемые свойства указанного типа</param>
        /// <returns></returns>
        private static object? DeserializeCsvLine(Type type, string str, string[] csvProps, Dictionary<string, PropertyInfo> expectedProperties)
        {
            string[] values = GetValuesFromCsvLine(str).ToArray();
            if (values.Length == csvProps.Length)
            {
                var result = Activator.CreateInstance(type);

                for (int i = 0; i < csvProps.Length; i++)
                {
                    if (!string.IsNullOrEmpty(values[i]) && expectedProperties.TryGetValue(csvProps[i], out PropertyInfo propInfo))
                    {
                        switch (propInfo.PropertyType.Name)
                        {
                            case "Byte[]":
                                propInfo.SetValue(result, Convert.FromBase64String(values[i]));
                                break;
                            default:
                                propInfo.SetValue(result, Convert.ChangeType(values[i], propInfo.PropertyType));
                                break;
                        }
                    }
                }

                return result;
            }
            else
            {
                throw new ArgumentException("Переданная строка не соответствует указанным свойствам по числу параметров либо формат CSV некорректный", nameof(str));
            }
        }

        /// <summary>
        /// Метод десериализации строки CSV в объект
        /// </summary>
        /// <typeparam name="T">Тип данных в который необходимо десериализовать строку</typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T? DeserializeFromCsv<T>(this string str)
        {
            List<string> lines = GetLines(str, true).ToList();
            if (lines.Count > 1)
            {
                Type type = typeof(T);

                string[] csvProps = lines[0].Split(','); // имена полей, определённые в первой строчке csv

                if (type.IsArray)
                {
                    // ожидается коллекция, значит мы должны пройтись по всем строчкам csv и заполнить её
                    var result = Activator.CreateInstance(type, lines.Count - 1) as IList;
                    var elementsType = type.GetElementType();
                    Dictionary<string, PropertyInfo> properties = elementsType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanWrite).ToDictionary(p => p.Name, p => p);

                    Parallel.For(1, lines.Count, i =>
                    {
                        var obj = DeserializeCsvLine(elementsType, lines[i], csvProps, properties);
                        lock (result)
                        {
                            result[i - 1] = obj;
                        }
                    });

                    return (T)result;
                }
                else
                {
                    // ожидаем модель данных, не коллекцию. Парсим только вторую строку для передачи данных в объект
                    return (T)DeserializeCsvLine(type, lines[1], csvProps, type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanWrite).ToDictionary(p => p.Name, p => p));
                }
            }

            return default;
        }
    }
}
