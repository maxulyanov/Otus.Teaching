﻿using System.Collections;
using System.Reflection;
using System.Text;

namespace CsvFormater
{
    public static class ToCsvExtensions
    {
        /// <summary>
        /// Вспомогательный метод для получения данных из свойства объекта
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName">имя свойства</param>
        /// <returns></returns>
        private static object? GetPropertyValue(this object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName)?.GetValue(obj, null);
        }

        /// <summary>
        /// Вспомогательный метод сериализации свойств линейного объекта в CSV формат. 
        /// </summary>
        /// <param name="obj">объект для рефлексивного анализа</param>
        /// <param name="properties">список известных свойст объекта</param>
        /// <returns></returns>
        private static string PropertiesToCsv(object obj, Dictionary<string, PropertyInfo> properties)
        {
            StringBuilder sb = new();

            foreach (var p in properties)
            {
                var propValue = obj.GetPropertyValue(p.Key);
                string value;
                switch (p.Value.PropertyType.Name)
                {
                    case "Byte[]":
                        if (propValue is byte[] arr)
                        {
                            value = $"{Convert.ToBase64String(arr)},";
                        }
                        else
                        {
                            value = $",";
                        }

                        break;
                    case "String":
                        value = $"\"{propValue}\",";
                        break;
                    default:
                        value = $"{propValue},";
                        break;
                }
                sb.Append(value);
            };

            sb.Remove(sb.Length - 1, 1); // удаляем лишнюю запятую
            return sb.ToString();
        }

        /// <summary>
        /// Метод сериализации свойств объекта в CSV формат. 
        /// </summary>
        /// <param name="obj">объект для рефлексивного анализа</param>
        /// <returns>CSV строка</returns>
        public static string SerializeToCsv(this object obj)
        {
            var objectType = obj.GetType();
            if (objectType.IsClass)
            {
                StringBuilder sb = new();

                if (!objectType.IsArray)
                {
                    Dictionary<string, PropertyInfo> properties = objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanRead).ToDictionary(p => p.Name, p => p);
                    if (properties.Count > 0)
                    {
                        sb.AppendLine(properties.Keys.ToArray().Aggregate((a, b) => $"{a},{b}"));
                        sb.AppendLine(PropertiesToCsv(obj, properties));
                    }
                }
                else
                {
                    // наш рефлексируемый объект является массивом

                    object syncLock = new();
                    var count = obj.GetPropertyValue("Length");

                    if (count is int intCount && obj is IList a)
                    {
                        var elementsType = objectType.GetElementType();
                        Dictionary<string, PropertyInfo> properties = elementsType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanRead).ToDictionary(p => p.Name, p => p);
                        if (properties.Count > 0)
                        {
                            sb.AppendLine(properties.Keys.ToArray().Aggregate((a, b) => $"{a},{b}"));

                            Parallel.For(0, intCount, (i) =>
                            {
                                string csv = PropertiesToCsv(a[i], properties);
                                if (!string.IsNullOrEmpty(csv))
                                {
                                    lock (syncLock)
                                    {
                                        sb.AppendLine(csv);
                                    }
                                }
                            });
                        }
                    }
                }

                return sb.ToString();
            }

            return string.Empty;
        }
    }
}
