﻿using eBay.CLI.Services;
using eBay.Infrastructure.Services;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace eBay.CLI
{
    /// <summary>
    /// Основной сервис приложения
    /// </summary>
    public class DbWorker : BackgroundService, IDisposable
    {
        readonly ILogger<DbWorker> _logger;
        readonly IDataGeneratorService _dataGenerator;
        readonly IDataPresentationService _dataPresentation;
        readonly IDataWriterService _dataWriter;
        public DbWorker(IDataPresentationService dataPresentation, IDataWriterService dataWriter, IDataGeneratorService dataGenerator, ILogger<DbWorker> logger)
        {
            _logger = logger;
            _dataGenerator = dataGenerator;
            _dataPresentation = dataPresentation;
            _dataWriter = dataWriter;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Console.Write("Генерация данных...");
            await _dataGenerator.FillDataBaseWithMockData(5, 2);

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    _dataPresentation.ShowHelp();
                    switch (Console.ReadLine())
                    {
                        case "1":
                            _dataPresentation.ShowUsersTable();
                            break;
                        case "2":
                            _dataPresentation.ShowItemsTable();
                            break;
                        case "3":
                            _dataPresentation.ShowBetsTable();
                            break;
                        case "4":
                            await _dataWriter.AddNewUser();
                            break;
                        case "5":
                            await _dataWriter.AddNewItem();
                            break;
                        case "6":
                            await _dataWriter.AddNewBet();
                            break;
                        default:
                            continue;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "DbWorker.ExecuteAsync: main error");
                }

                Console.Write("\r\nДля возврата нажмите любую клавишу...");
                Console.ReadKey();
            }
        }
    }
}
