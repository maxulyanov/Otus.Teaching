﻿using eBay.CLI.Services;
using eBay.Infrastructure.Extensions;
using eBay.Infrastructure.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;

namespace eBay.CLI
{
    class Program
    {
        public static IConfiguration Configuration { get; } = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ENVIRONMENT")}.json", true)
                .AddEnvironmentVariables().Build();

        static void Main(string[] args)
        {
            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(Configuration).CreateLogger();

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var host = Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureServices((context, services) =>
                {
                    var connStr = Configuration.GetConnectionString("PgConnection");
                    services.AddHostedService<DbWorker>();
                    services.AddNHibernate(connStr);
                    services.AddTransient<IDataGeneratorService, DataGeneratorService>();
                    services.AddTransient<IDataPresentationService, DataPresentationService>();
                    services.AddTransient<IDataWriterService, DataWriterService>();
                });

            return host;
        }
    }
}
