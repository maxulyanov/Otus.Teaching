﻿using eBay.Domain.AggregatesModel;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace eBay.Infrastructure.Map
{

    public class BetMap : ClassMapping<Bet>
    {
        /// <summary>
        /// Маппинг данных сущности <seealso cref="Bet"/>
        /// </summary>
        public BetMap()
        {
            Table("Bets");

            Id(bet => bet.Id, map =>
            {
                map.Generator(Generators.SequenceIdentity);
                map.Type(NHibernateUtil.Int32);
                map.UnsavedValue(0);
            });

            Property(bet => bet.Created, map =>
            {
                map.Type(NHibernateUtil.DateTime);
                map.NotNullable(true);
            });

            Property(bet => bet.BetValue, map =>
            {
                map.Type(NHibernateUtil.Decimal);
            });

            ManyToOne(bet => bet.Item, map => map.Column("ItemId"));
            ManyToOne(bet => bet.Owner, map => map.Column("UserId"));
        }
    }
}
