﻿using eBay.Domain.AggregatesModel;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace eBay.Infrastructure.Map
{
    public class UserMap : ClassMapping<User>
    {
        /// <summary>
        /// Маппинг данных сущности <seealso cref="User"/>
        /// </summary>
        public UserMap()
        {
            Table("Users");

            Id(user => user.Id, map =>
            {
                map.Generator(Generators.SequenceIdentity);
                map.Type(NHibernateUtil.Int32);
                map.UnsavedValue(0);         
            });

            Property(user => user.Name, map =>
            {
                map.Type(NHibernateUtil.String);
                map.Length(150);
                map.NotNullable(true);
                map.Unique(true);
            });

            Property(user => user.Email, map =>
            {
                map.Type(NHibernateUtil.String);
                map.Length(100);
            });

            Bag(user => user.Items, map => map.Key(k => k.Column("ItemId")), rel => rel.OneToMany());
            Bag(user => user.Bets, map => map.Key(k => k.Column("UserId")), rel => rel.OneToMany());
        }
    }
}
