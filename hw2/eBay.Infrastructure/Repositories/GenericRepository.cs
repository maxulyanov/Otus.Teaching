﻿using eBay.Domain.Common;
using NHibernate;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace eBay.Infrastructure.Repositories
{
    public class GenericRepository<T> : IRepository<T> where T : Entity
    {
        readonly ISession _session;

        public GenericRepository(ISession session)
        {
            _session = session;
        }

        public async Task Delete(T entity)
        {
            await _session.DeleteAsync(entity);
        }

        public async Task<T> Find(object id)
        {
            return await _session.GetAsync<T>(id);
        }

        public IQueryable<T> Get(ISpecification<T> filter = null)
        {
            return filter == null ? _session.Query<T>() : _session.Query<T>().Where(filter.SpecExpression);
        }

        public async Task InsertOrUpdate(T entity)
        {
            await _session.SaveOrUpdateAsync(entity);
        }

        #region IDisposable

        private bool _disposed;
        protected void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                _session?.Dispose();
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
