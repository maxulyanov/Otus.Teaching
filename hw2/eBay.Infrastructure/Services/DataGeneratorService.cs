﻿using Bogus;
using eBay.Domain.AggregatesModel;
using eBay.Infrastructure.Map;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eBay.Infrastructure.Services
{
    /// <summary>
    /// Сервис генерации данных
    /// </summary>
    public interface IDataGeneratorService
    {
        /// <summary>
        /// Метод генерирует указанное число пользователей
        /// </summary>
        /// <param name="count">Количество пользователей для генерации</param>
        /// <returns></returns>
        public IEnumerable<User> GenerateUsers(int count);

        /// <summary>
        /// Метод генерирует указанное число лотов для пользователя
        /// </summary>
        /// <param name="owner">Владельца лота</param>
        /// <param name="itemsCount">Количество лотов для генерации</param>
        /// <returns></returns>
        public IEnumerable<Item> GenerateItems(User owner, int itemsCount);

        /// <summary>
        /// Метод генерирует по одной ставке для каждого пользователя
        /// </summary>
        /// <param name="item">Лот для генерации ставок</param>
        /// <param name="forUsers">Список пользователей для которых будет сгенерирована ставка</param>
        /// <returns></returns>
        public IEnumerable<Bet> GenerateBets(Item item, IEnumerable<User> forUsers);

        /// <summary>
        /// Метод заполняет базу данных фейковыми данными
        /// </summary>
        /// <param name="usersCount">Количество пользователей в БД</param>
        /// <param name="itemsCount">Количество лотов в БД</param>
        Task FillDataBaseWithMockData(int usersCount, int itemsCount);

    }

    public class DataGeneratorService : IDataGeneratorService
    {
        readonly IDataContext _session;
        readonly ILogger<DataGeneratorService> _logger;
        public DataGeneratorService(IDataContext session, ILogger<DataGeneratorService> logger)
        {
            _session = session;
            _logger = logger;
        }

        public IEnumerable<Bet> GenerateBets(Item item, IEnumerable<User> forUsers)
        {
            if (item == null)
                throw new ArgumentException("Лот не указан", nameof(item));

            if (forUsers?.Count() > 0)
            {

                List<Bet> result = new();
                Random rnd = new();
                decimal value = item.CurrentValue;
                
                foreach (User u in forUsers)
                {
                    var fakeBet = new Faker<Bet>().CustomInstantiator(f => Bet.Create(0, u, item, f.Random.Decimal(item.CurrentValue + 1, item.CurrentValue + 10)));
                    result.Add(fakeBet.Generate());
                }

                return result;
            }

            return Enumerable.Empty<Bet>();
        }

        public IEnumerable<Item> GenerateItems(User owner, int itemsCount)
        {
            if (owner == null)
                throw new ArgumentException("Владелец не указан", nameof(owner));

            if (itemsCount > 0)
            {
                List<Item> result = new();
                var fakeItem = new Faker<Item>()
                        .CustomInstantiator(f =>
                        Item.Create(0,
                        f.Commerce.ProductName(),
                        f.Commerce.ProductDescription(),
                        decimal.Parse(f.Commerce.Price()),
                        f.Date.Future(refDate: DateTime.UtcNow.AddDays(1)).ToUniversalTime(),
                        owner));

                for (int i = 0; i < itemsCount; i++)
                {
                    result.Add(fakeItem.Generate());
                }

                return result;
            }

            return Enumerable.Empty<Item>();
        }

        public IEnumerable<User> GenerateUsers(int count)
        {
            if (count > 0)
            {
                List<User> result = new();

                var fakeUser = new Faker<User>().CustomInstantiator(f => User.Create(0, f.Person.UserName, f.Person.Email));

                for (int i = 0; i < count; i++)
                {
                    result.Add(fakeUser.Generate());
                }

                return result;
            }

            return Enumerable.Empty<User>();
        }

        public async Task FillDataBaseWithMockData(int usersCount, int itemsCount)
        {
            try
            {
                _session.BeginTransaction();
                List<User> userList = GenerateUsers(usersCount).ToList();
                userList.ForEach(u =>
                {
                    _session.UserRepository.InsertOrUpdate(u).Wait();
                });

                userList.ForEach(u =>
                {
                    List<Item> itemList = GenerateItems(u, itemsCount).ToList();
                    itemList.ForEach(i =>
                    {
                        _session.ItemRepository.InsertOrUpdate(i).Wait();
                        
                        List<Bet> betList = GenerateBets(i, userList).ToList();
                        betList.ForEach(b =>
                        {
                            try
                            {
                                i.Bet(b);
                                _session.BetRepository.InsertOrUpdate(b).Wait();                                
                            }
                            catch { /*Игнорируем неверные ставки*/ }
                        });
                    });
                });

                //сохраняем транзакцию
                await _session.Commit();
            }
            catch (Exception ex)
            {
                await _session.Rollback();
                _logger.LogError(ex, "DataGeneratorService.FillDataBaseWithMockData");
            }
            finally
            {
                _session.CloseTransaction();
            }
        }
    }
}
