﻿using eBay.Domain.AggregatesModel;
using eBay.Domain.Common;
using eBay.Infrastructure.Repositories;
using NHibernate;
using System;
using System.Threading.Tasks;

namespace eBay.Infrastructure.Map
{
    public interface IDataContext : IUnitOfWork
    {
        IRepository<Bet> BetRepository { get; }
        IRepository<Item> ItemRepository { get; }
        IRepository<User> UserRepository { get; }
    }

    public class DataUnitOfWork : IDataContext
    {
        private readonly ISession _session;
        private ITransaction _transaction;
        private GenericRepository<Bet> _betRepository;
        private GenericRepository<Item> _itemRepository;
        private GenericRepository<User> _userRepository;

        public DataUnitOfWork(ISession session)
        {
            _session = session;
        }

        public IRepository<Bet> BetRepository => _betRepository ??= new GenericRepository<Bet>(_session);

        public IRepository<Item> ItemRepository => _itemRepository ??= new GenericRepository<Item>(_session);

        public IRepository<User> UserRepository => _userRepository ??= new GenericRepository<User>(_session);


        public void BeginTransaction()
        {
            _transaction = _session.BeginTransaction();
        }

        public async Task Commit()
        {
            await _transaction.CommitAsync();
        }

        public async Task Rollback()
        {
            await _transaction.RollbackAsync();
        }

        public void CloseTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public async Task Save(IAggregateRoot entity)
        {
            await _session.SaveOrUpdateAsync(entity);
        }

        public async Task Delete(IAggregateRoot entity)
        {
            await _session.DeleteAsync(entity);
        }

        private bool _disposed;

        protected void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                _session?.Dispose();
                _betRepository?.Dispose();
                _itemRepository?.Dispose();
                _userRepository?.Dispose();
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
