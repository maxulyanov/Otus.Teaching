﻿-- Database: ebay

-- DROP DATABASE IF EXISTS ebay;

CREATE DATABASE ebay
    WITH 
    OWNER = ebay
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE ebay
    IS 'home work 2';