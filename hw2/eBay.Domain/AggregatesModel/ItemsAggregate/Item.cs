﻿using eBay.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eBay.Domain.AggregatesModel
{
    /// <summary>
    /// Сущность лота аукциона
    /// </summary>
    public class Item : Entity
    {
        private List<Bet> _betList = new();
        /// <summary>
        /// Название лота
        /// </summary>
        public virtual string Name { get; set; }
        /// <summary>
        /// Описание лота
        /// </summary>
        public virtual string Description { get; set; }
        /// <summary>
        /// Стартовое значение стоимости лота
        /// </summary>
        public virtual decimal CurrentValue { get; set; }
        /// <summary>
        /// Дата и время закрытия торгов
        /// </summary>
        public virtual DateTime CloseDateUtc { get; set; }
        /// <summary>
        /// Ставки
        /// </summary>
        public virtual IEnumerable<Bet> Bets { get => _betList; set => _betList = value.ToList(); }
        /// <summary>
        /// Хозяин лота
        /// </summary>
        public virtual User Owner { get; set; }

        /// <summary>
        /// Метод создания лота
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="name">Название лота</param>
        /// <param name="description">Описание лота</param>
        /// <param name="startValue">Начальная стоимость лота</param>
        /// <param name="closeDateUtc">Время закрытия торгов за лот</param>
        /// <param name="owner">Владелец лота</param>
        /// <returns></returns>
        public static Item Create(int id, string name, string description, decimal startValue, DateTime closeDateUtc, User owner)
        {
            if (owner is null)
                throw new ArgumentNullException(nameof(owner), "Item.Create: Владелец лота не установлен");
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Item.Create: Значение не может быть пустым", nameof(name));
            if (startValue < 0)
                throw new ArgumentException("Item.Create: Значение не может быть отрицательным", nameof(startValue));
            if (closeDateUtc < DateTime.UtcNow.AddDays(1))
                throw new ArgumentException("Item.Create: Время торгов не может быть менее 24 часов", nameof(closeDateUtc));

            var item = new Item
            {
                Id = id,
                CloseDateUtc = closeDateUtc,
                Description = description,
                Name = name,
                Owner = owner,
                CurrentValue = startValue
            };

            //TODO Событие создания лота
            return item;
        }

        /// <summary>
        /// Метод обновления ставки
        /// </summary>
        /// <param name="bet">Новая ставка</param>
        public virtual void Bet(Bet bet)
        {
            if (bet is null)
                throw new ArgumentNullException(nameof(bet), "Item.Bet: Ставка не может быть пустой");
            if (bet.BetValue <= this.CurrentValue)
                throw new ArgumentOutOfRangeException(nameof(bet), "Item.Bet: нельзя применить указанную ставку. Увеличьте её значение");

            //TODO Событие обновления ставки
            this._betList.Add(bet);
        }
    }
}
