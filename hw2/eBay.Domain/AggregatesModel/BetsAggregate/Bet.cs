﻿using eBay.Domain.Common;
using System;

namespace eBay.Domain.AggregatesModel
{
    /// <summary>
    /// Сущность ставки пользователя на аукционе за какой-то лот
    /// </summary>
    public class Bet : Entity
    {
        /// <summary>
        /// Ставка в условных единицах
        /// </summary>
        public virtual decimal BetValue { get; set; }

        /// <summary>
        /// Пользователь, сделавший ставку
        /// </summary>
        public virtual User Owner { get; set; }

        /// <summary>
        /// Предмет ставки
        /// </summary>
        public virtual Item Item { get; set; }

        /// <summary>
        /// Время создания ставки
        /// </summary>
        public virtual DateTime Created { get; set; }

        /// <summary>
        /// Метод создания ставки
        /// </summary>
        /// <param name="id">Идентификатор ставки</param>
        /// <param name="user">Создатель ставки</param>
        /// <param name="item">Предмет на который сделана ставка</param>
        /// <param name="value">Значение ставки</param>
        /// <returns></returns>
        public static Bet Create(int id, User user, Item item, decimal value)
        {
            if (user is null)
                throw new ArgumentNullException(nameof(user), "Bet.Create: Пользователь не может быть пустым");
            if (item is null)
                throw new ArgumentNullException(nameof(item), "Bet.Create: Предмет ставки не может быть пустым");
            if (value <= item.CurrentValue)
                throw new ArgumentOutOfRangeException(nameof(value), "Bet.Create: Ставка не может быть меньше текущего или стартового значения");

            DateTime now = DateTime.UtcNow;
            if (now > item.CloseDateUtc)
                throw new ArgumentOutOfRangeException(nameof(item.CloseDateUtc), "Bet.Create: Ставка не может быть сделана, так как время вышло");

            var bet = new Bet
            {
                Id = id,
                BetValue = value,
                Created = now,
                Item = item,
                Owner = user
            };

            return bet;
        }
    }
}
