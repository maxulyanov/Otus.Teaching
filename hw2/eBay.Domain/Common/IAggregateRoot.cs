﻿namespace eBay.Domain.Common
{
    /// <summary>
    /// Базовый интерфейс доменной сущности для наследования и имплементации
    /// </summary>
    public interface IAggregateRoot
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        int Id { get; }
    }
}
