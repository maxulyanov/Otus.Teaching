﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace eBay.Domain.Common
{
    public interface IRepository<TEntity>: IDisposable where TEntity : IAggregateRoot
    {
        Task<TEntity> Find(object id);
        Task InsertOrUpdate(TEntity entity);
        Task Delete(TEntity entity);
        IQueryable<TEntity> Get(ISpecification<TEntity> filter = null);
    }
}
