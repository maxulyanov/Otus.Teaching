﻿using System;
using System.Linq.Expressions;

namespace eBay.Domain.Common
{
    /// <summary>
    /// Базовый интерфес паттерна "спеецификация" для имплементации условий
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public interface ISpecification<T>
    {
        /// <summary>
        /// Выражение спецификации, поиск или прочее условие
        /// </summary>
        Expression<Func<T, bool>> SpecExpression { get; }

        /// <summary>
        /// Метод определения соответствия сущности требуемой спецификации
        /// </summary>
        /// <param name="obj">Объект сущности</param>
        /// <returns></returns>
        bool IsSatisfiedBy(T obj);
    }

    /// <summary>
    /// Базовый класс для определения паттерна "спецификация" для сущности
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public abstract class SpecificationBase<T> : ISpecification<T>
    {
        /// <summary>
        /// приватный делегат для хранения скомпилированного лябда-выражения спецификации
        /// </summary>
        private Func<T, bool> _compiledExpression;

        /// <summary>
        /// Выражение, которое следует определить в производном классе
        /// </summary>
        public abstract Expression<Func<T, bool>> SpecExpression { get; }

        /// <summary>
        /// Делегат выражения спецификации
        /// </summary>
        public Func<T, bool> CompiledExpression => _compiledExpression ?? (_compiledExpression = SpecExpression.Compile());
        public bool IsSatisfiedBy(T obj) => CompiledExpression(obj);
    }
}
