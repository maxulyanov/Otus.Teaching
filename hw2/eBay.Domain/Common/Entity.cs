﻿using System;

namespace eBay.Domain.Common
{
    /// <summary>
    /// Базовый класс сущности домена
    /// </summary>
    public abstract class Entity : IAggregateRoot
    {
        int? _requestedHashCode;
        int _Id;

        public virtual int Id { get => _Id; set => _Id = value; }

        /// <summary>
        /// The instance is not, and has never been associated with any persistence context. It has no persistent identity (primary key value)
        /// </summary>
        /// <returns></returns>
        public virtual bool IsTransient()
        {
            return this.Id == default;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Entity))
                return false;
            if (Object.ReferenceEquals(this, obj))
                return true;
            if (this.GetType() != obj.GetType())
                return false;
            Entity item = (Entity)obj;
            if (item.IsTransient() || this.IsTransient())
                return false;
            else
                return item.Id == this.Id;
        }

        public override int GetHashCode()
        {
            if (!IsTransient())
            {
                if (!_requestedHashCode.HasValue)
                    _requestedHashCode = this.Id.GetHashCode() ^ 31;

                return _requestedHashCode.Value;
            }
            else
                return base.GetHashCode();
        }

        public static bool operator ==(Entity left, Entity right)
        {
            if (Object.Equals(left, null))
                return (Object.Equals(right, null));
            else
                return left.Equals(right);
        }

        public static bool operator !=(Entity left, Entity right)
        {
            return !(left == right);
        }
    }
}
