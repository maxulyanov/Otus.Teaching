using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        readonly IDataContext _context;
        readonly ILogger<CustomerController> _logger;

        public CustomerController(IDataContext context, ILogger<CustomerController> logger)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet("{id:long}")]   
        public ActionResult<Customer> GetCustomerAsync([FromRoute] long id)
        {
            Customer customer = _context.CustomerFind(id);
            if (customer==null)
            {
                return NotFound();
            }

            return customer;
        }

        [HttpPost("")]   
        public ActionResult<long> CreateCustomerAsync([FromBody] Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (_context.CustomerFind(customer.Id) != null)
            {
                return StatusCode((int)HttpStatusCode.Conflict); // return 409 code
            }

            long result = _context.CustomerAdd(customer);
            if (result>0)
            {
                return Ok(result);
            }

            return StatusCode((int)HttpStatusCode.InternalServerError); // return 500 code
        }
    }
}