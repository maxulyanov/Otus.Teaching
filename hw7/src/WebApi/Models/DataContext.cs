﻿using LiteDB;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;

namespace WebApi.Models
{
    public interface IDataContext
    {
        /// <summary>
        /// Добавляем клиента
        /// </summary>
        /// <param name="customer">Модель данных клиента</param>
        /// <returns>Идентификатор нового клиента</returns>
        long CustomerAdd(Customer customer);

        /// <summary>
        /// Ищем клиента в БД
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns>Модель данных клиента</returns>
        Customer CustomerFind(long id);

    }
    /// <summary>
    /// Контекст данных
    /// </summary>
    class DataContext: IDataContext
    {
        private readonly LiteDatabase _db;
        private ILogger<DataContext> _logger;

        public DataContext(IConfiguration config, ILogger<DataContext> logger)
        {
            _db = new LiteDatabase(config.GetConnectionString("default"));
            _logger = logger;
        }

        public long CustomerAdd(Customer customer)
        {
            if (customer != null)
            {
                try
                {
                    var entity = _db.GetCollection<Customer>("Customers");
                    var result = entity.Insert(customer);
                    _db.Commit();

                    return (long)result.RawValue;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "CustomerAdd error");
                }
            }

            return 0;
        }

        public Customer CustomerFind(long id)
        {
            try
            {
                var entity = _db.GetCollection<Customer>("Customers");
                var result = entity.FindById(new BsonValue(id));

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "CustomerFind error");
            }

            return default;
        }
    }
}
