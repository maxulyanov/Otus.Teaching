﻿
using events;
using events.Extensions;
using System.Collections.Concurrent;

// объект для поиска файлов п.2,3,4 задания
IFileSearcher fileSearcher = new FileSearcher();

// определяем коллекцию для п.1 задания
ConcurrentBag<string> fileList = new();

string searchPath = args?.Length > 0 ? args[0] : Environment.CurrentDirectory;
string searchPattern = args?.Length > 1 ? args[1] : "*";

// функция/делегат определения размера файла для п.1,5 задания
Func<string, float> getFileSize = (string filePath) =>
{
    return new FileInfo(filePath).Length;
};

fileSearcher.FileFound += (s, e) =>
{
    if (e is FileArgs arg)
    {
        fileList.Add(arg.FilePath);
        Console.WriteLine($"{arg.FilePath}");
    }
};

// запускаем асинхронный поиск файлов
fileSearcher.RunSearchAsync(searchPath, searchPattern);

Console.WriteLine("Для прекращения поиска нажмите любую клавишу...");

while(fileSearcher.isRunning)
{
    Task.Delay(100).Wait();
    if (Console.KeyAvailable)
    {
        fileSearcher.StopSearch();
    }
}

// останавливаем поиск если запущен, освобождаем ресурсы
fileSearcher.Dispose();

Console.WriteLine("Поиск завершён");

// выводим файл с наибольшим размером
if (!fileList.IsEmpty)
{
    var m = fileList.GetMax(getFileSize);
    Console.WriteLine($"Максимальный размер у файла '{m}'");
}
else
{
    Console.WriteLine("Ниодного файла не найдено");
}